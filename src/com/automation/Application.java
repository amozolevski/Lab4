/*
Один интерфейс(Animal) и два наследника(Cat & Dog)
 */

package com.automation;

public class Application {
    public static void main(String[] args){
        new Cat().makeSound();
        new Dog().makeSound();
    }
}
